aflplusplus (4.02c-1) unstable; urgency=medium

  * New upstream version 4.02c
  * Switch to llvm-toolchain-14 (Closes: #1017655)
  * Do not run dh_dwz
  * Update lintian-overrides
  * Bump Standards-Version to 4.6.1 (no change)

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 08 Sep 2022 15:02:46 +0200

aflplusplus (4.00c-1) unstable; urgency=medium

  * Switch to llvm-toolchain-13 (Closes: #1004881)
  * New upstream version 4.00c
  * Update debian/copyright
  * Refresh the patch
  * Update doc installation
  * Update lintian-overrides
  * Add a patch to fix spelling errors

 -- Sophie Brun <sophie@offensive-security.com>  Mon, 07 Feb 2022 14:54:56 +0100

aflplusplus (3.14c-2) unstable; urgency=medium

  * Add correctly the helper-scripts afl-clang and afl-clang++ (issue with
    upstream .gitignore)

 -- Sophie Brun <sophie@offensive-security.com>  Wed, 15 Sep 2021 09:06:42 +0200

aflplusplus (3.14c-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + afl++-doc: Add Multi-Arch: foreign.

  [ Sophie Brun ]
  * New upstream version 3.14c
  * Add missing dependency procps to afl++
  * Add new build-dep: gcc-10-plugin-dev
  * Update debian/copyright
  * debian/rules: update build for new release
  * Add build-deps for tests: clang and libcmocka-dev
  * Install all files in afl-++. afl++-clang is now a transitional package.
  * debian/rules: no need to remove non -fast binaries
  * Add a patch to fix the failing test
  * Update debian/afl++-clang.lintian-overrides
  * Bump Standards-Version to 4.6.0 (no change)
  * Add scripts for deprecated afl-clang and afl-clang++ with specific clang version
  * Use clang and llvm version 12

 -- Sophie Brun <sophie@offensive-security.com>  Tue, 14 Sep 2021 08:41:43 +0200

aflplusplus (2.68c-1) unstable; urgency=medium

  * New upstream version 2.68c
  * Add a patch to improve reproducibility (Closes: #969320)
  * Improve long description to make it clear that afl is unmaintained
    (Closes: #973557)
  * Switch to clang/LLVM 11 (Closes: #974783)
  * Drop all patches, they have been merged upstream.

 -- Raphaël Hertzog <raphael@offensive-security.com>  Tue, 17 Nov 2020 12:06:15 +0100

aflplusplus (2.67c-1) unstable; urgency=medium

  [ Sophie Brun ]
  * Revert "Switch to clang 11"
  * Fix broken symlinks (Closes: #968263)

  [ Raphaël Hertzog ]
  * New upstream version 2.67c
    - this versions disables -march=native when SOURCE_DATE_EPOCH is set
      Closes: #968336
  * Update patches for new upstream release
  * Fix installation path of helper libraries (Closes: #969151)
  * Switch to debhelper compat 13
  * Switch to clang/llvm 10
  * Install the upstream manual pages
  * Update Add-missing-CPPFLAGS.patch to cover afl-performance.o too

 -- Raphaël Hertzog <raphael@offensive-security.com>  Fri, 28 Aug 2020 21:15:22 +0200

aflplusplus (2.66c-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Rely on LLVM_CONFIG environment variable to force usage of a specific
    CLANG version
  * Don't duplicate LLVM major version in binary package dependencies

  [ Samuel Henrique ]
  * Configure git-buildpackage for Debian

  [ Sophie Brun ]
  * New upstream version 2.66c (Closes: #956986)
  * Remove unneeded patches
  * Switch to clang 11
  * Update build and installation for new release
  * Wrap-and-sort
  * Update debian/copyright
  * Update source/lintian-overrides
  * Add transitional packages (Closes: #942291)
  * Add a patch to fix spelling errors
  * Don't install upstream installation documentation
  * Patch to add missing CPPFLAGS
  * Bump Standards-Version to 4.5.0 (no change)
  * Re-enable build log scanner as #948009 is fixed
  * Add a patch to fix the file-references-package lintian info

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 07 Aug 2020 11:48:29 +0200

aflplusplus (2.60c-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.60c
  * Remove one patch merged upstream
  * Stop cleaning what upstream cleans for us now
  * Fix version of clang dependency
  * Update patch to cover another llvm-config invocation
  * Run tests at build time
  * Disable build log scanner until #948009 is fixed
  * Fix autopkgtests

 -- Raphaël Hertzog <hertzog@debian.org>  Fri, 03 Jan 2020 11:21:35 +0100

aflplusplus (2.59c-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.4.1
  * Add new patch to support lack of unicorn_mode/unicorn directory
  * Remove binary file created by make clean (duh!)
  * Switch to clang 9.
  * Add a new patch fixing the build of libdislocator
  * Fix path of documentation files to install
  * Enable python 3 support

 -- Raphaël Hertzog <raphael@offensive-security.com>  Mon, 30 Dec 2019 14:12:46 +0100

aflplusplus (2.53c-1) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Team upload.
  * Fix Vcs-Git and Vcs-Browser URLs.
  * Note for ftpmasters: the source package contains a number of binary-only
    files in the "testcases" and "docs/vuln_samples/" subdirectories. We
    believe that they are their own sources as they have been manually crafted
    to meet specific requirements. Those files were already part of the former
    "afl" package that recently got removed. Thank you.

  [ Sophie Brun ]
  * Initial releases (Closes: #934964)
  * Add more info in patches

 -- Raphaël Hertzog <hertzog@debian.org>  Tue, 17 Sep 2019 10:52:07 +0200
